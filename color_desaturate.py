#!/usr/bin/env python
# coding=utf-8
from __future__ import absolute_import, division

import coloreffect


class C(coloreffect.ColorEffect):
    def colmod(self, r, g, b):
        l = (max(r, g, b) + min(r, g, b)) // 2
        ig = int(round(l))
        return '{:02x}{:02x}{:02x}'.format(ig, ig, ig)


if __name__ == '__main__':
    C().run()
